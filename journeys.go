package main

import (
	"fmt"
	"sync"
)

type Journeys struct {
	// groups is a Map in which keys are group ids and values are journey structs
	groups sync.Map
	// waitlist is a Queue to store the journeys/groups not assigned to a car
	waitlist Queue
	// seatsData is a ptr to a Seats structure in order to check for seats availability
	seatsData *Seats
}

// journey is a struct that stores a group (id and number of people), the travel car id
// assigned to that group or 0 and if there is not a car assigned a pointer to
// the current position in the waitlist for the group
type journey struct {
	group     Group
	travelCar uint32
	waitPos   *Node
}

// Group represents the json items expected in the POST /journey endpoint
type Group struct {
	Id     uint32 `json:"id"`
	People uint8  `json:"people"`
}

func (g *Group) validate(j *Journeys) (err error) {
	if g.Id < 1 {
		return fmt.Errorf("Group with wrong id %d", g.Id)
	}
	if g.People < 1 || g.People > 6 {
		return fmt.Errorf("Expected 1 to 6 peoople in group %d, but was %d", g.Id, g.People)
	}
	if _, found := j.groups.Load(g.Id); found {
		return fmt.Errorf("Group with id %d already in a journey", g.Id)
	}
	return err
}

// New creates a new instance of Journeys and assign a ptr to the data related with seats
func New(seats *Seats) (j *Journeys) {
	if seats != nil {
		j = &Journeys{}
		j.seatsData = seats
	}
	return j
}

// Add creates a new instance of a journey using a group and tries to BookCar if there is
// any available. If no car is found, the journey is pushed to the waitlist
func (j *Journeys) Add(g Group) (err error) {
	if err = g.validate(j); err != nil {
		return err
	}
	var newJourney = journey{g, 0, nil}
	var availCar, found = j.seatsData.BookCar(g.People)
	if found {
		newJourney.travelCar = availCar
	} else {
		//log.Printf(" Car not found for group %d of %d people", g.Id, g.People)
		newJourney.waitPos = j.waitlist.Push(&newJourney)
	}
	j.groups.Store(g.Id, newJourney)
	return err
}

// get returns if there is a journey for a given groupId. If the group is found
// the bool return is true
func (j *Journeys) get(groupId uint32) (*journey, bool) {
	var result journey
	var g, found = j.groups.Load(groupId)
	if found {
		result = g.(journey)
	}
	return &result, found
}

// GetCarID returns the car assigned to a group if found
func (j *Journeys) GetGroupCar(groupId uint32) (carGroup *Car, found bool) {
	journey, found := j.get(groupId)
	if found {
		carGroup, _ = j.seatsData.GetCar(journey.travelCar)
	}
	return carGroup, found
}

// DropGroup removes a specific group from the groups Map. If the group had a car
// assigned, uses the available seats to check if they could be used by other waiting groups
func (j *Journeys) DropGroup(groupId uint32) (err error) {
	journey, found := j.get(groupId)
	if !found {
		return fmt.Errorf("Group with %d not found", groupId)
	}
	if journey.travelCar == 0 {
		j.waitlist.Delete(journey.waitPos)
	} else {
		var freeSeats = j.checkWaitlist(journey.travelCar, journey.group.People)
		j.seatsData.Free(journey.travelCar, freeSeats)
	}
	j.groups.Delete(journey.group.Id)
	return err
}

// checkWaitlist iterates over the groups in the waitlist to check if
// seats available in a car due to a dropoff, fit one or several waiting groups.
// Returns the remaining seats available.
func (j *Journeys) checkWaitlist(carId uint32, availSeats uint8) (freeSeats uint8) {

	var currentSeats, _ = j.seatsData.ByCarId(carId)
	freeSeats = currentSeats + availSeats
	var node = j.waitlist.Peek()
	for node != nil && freeSeats > 0 {
		var journey = node.data.(*journey)
		if journey.group.People <= freeSeats {
			journey.travelCar = carId
			freeSeats -= journey.group.People
			j.waitlist.Delete(node)
			journey.waitPos = nil
			j.groups.Store(journey.group.Id, *journey)
		}
		node = node.next
	}
	return freeSeats
}

//Clear Initializes all the fields of the Journeys struct to nil
func (j *Journeys) Clear() {
	if j == nil {
		return
	}
	j.seatsData = nil
	j.waitlist.Clear()
	j.groups.Range(func(key interface{}, value interface{}) bool {
		j.groups.Delete(key)
		return true
	})
}

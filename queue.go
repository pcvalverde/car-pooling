package main

import (
	"fmt"
	"sync"
)

//Custom implementation of a concurrent FIFO Queue using a linked list of Node
//Can hold any data using the interface{} type, but casting is required
type Node struct {
	data interface{}
	next *Node
}

type Queue struct {
	head  *Node
	tail  *Node
	mutex sync.RWMutex
	size  uint32
}

func (q *Queue) Push(newData interface{}) *Node {
	q.mutex.Lock()
	defer q.mutex.Unlock()
	var newNode = &Node{newData, nil}
	if q.head == nil {
		q.head = newNode
		q.tail = newNode
		q.size = 1
	} else {
		q.tail.next = newNode
		q.tail = newNode
		q.size++
	}
	return newNode
}

func (q *Queue) Pop() interface{} {
	q.mutex.Lock()
	defer q.mutex.Unlock()
	var value interface{}
	if q.head != nil {
		value = q.head.data
		prev := q.head.next
		q.head = nil
		q.head = prev
		q.size--
	}
	return value
}

func (q *Queue) Size() uint32 {
	return q.size
}

func (q *Queue) Peek() *Node {
	q.mutex.Lock()
	defer q.mutex.Unlock()
	return q.head
}

func (q *Queue) Delete(node *Node) {
	q.mutex.Lock()
	defer q.mutex.Unlock()

	if node == nil || q.head == nil {
		return
	}

	if node == q.head && node == q.tail {
		node, q.head, q.tail = nil, nil, nil
		q.size = 0
		return
	}
	if node == q.head {
		q.head = q.head.next
		node = nil
		q.size--
		return
	}
	var prevNode = q.head

	for nextNode := prevNode.next; nextNode != nil; {
		if node == nextNode {
			prevNode.next = node.next
			node = nil
			q.size--
			if prevNode.next == nil {
				q.tail = prevNode
			}
			break
		} else {
			prevNode = nextNode
			nextNode = nextNode.next
		}
	}
}

func (q *Queue) Clear() {
	for q.head != nil {
		q.Pop()
	}
}

func (q *Queue) Display() {
	node := q.head
	fmt.Printf("Queue of size %d \n", q.size)
	for node != nil {
		fmt.Printf("%+v ->", node.data)
		node = node.next
	}
	fmt.Println()
}

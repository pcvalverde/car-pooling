package main

import (
	"sync"
	"testing"
)

func generateCars(size uint32) []Car {
	myCars := make([]Car, size)
	var id, i uint32 = 1, 0
	availability := []uint8{4, 5, 6}
	s := 0
	for ; i < size; i++ {
		myCars[i] = Car{id, availability[s]}
		s++
		id++
		if s == 3 {
			s = 0
		}
	}
	return myCars
}

func initialSeats() *Seats {
	var cars2add = []Car{
		{1, 4},
		{2, 5},
		{3, 6},
		{4, 4},
	}
	seats, _ := NewSeats(cars2add)
	return seats
}

func TestSeats(t *testing.T) {

	t.Run("Check initial availability", func(te *testing.T) {
		var mySeats = initialSeats()
		if mySeats.bySize[4].size != 2 || mySeats.bySize[5].size != 1 || mySeats.bySize[6].size != 1 {
			te.Errorf("Seats queue not intialize correctly")
		}
	})

	t.Run("Book several seats", func(te *testing.T) {
		var mySeats = initialSeats()
		var carId, _ = mySeats.BookCar(6)
		if carId != 3 {
			te.Errorf("Expected car with id 3, but was %d", carId)
		}
		carId, _ = mySeats.BookCar(2)
		if carId != 1 {
			te.Errorf("Expected car with id 1, but was %d", carId)
		}
		carId, _ = mySeats.BookCar(6)
		if carId != 0 {
			te.Errorf("Not expected a car with 6 seats")
		}
		if mySeats.bySize[4].size != 1 {
			te.Errorf("Expected 1 car with 4 availabke seats, but was %d ", mySeats.bySize[4].size)
		}
	})

	t.Run("Concurrent initialization for checking performance/race conditions", func(te *testing.T) {
		const routines = 100
		const totalCars = 100000
		var newCars = generateCars(totalCars)
		var wg sync.WaitGroup
		wg.Add(routines)
		var (
			concurrent = func() {
				defer wg.Done()
				var mySeats, _ = NewSeats(newCars)
				var count = mySeats.bySize[4].Size() + mySeats.bySize[5].Size() + mySeats.bySize[6].Size()
				if count != totalCars {
					te.Errorf("Expected %d seats and created %d", totalCars, count)
				}
			}
		)
		for i := 0; i < routines; i++ {
			go concurrent()
		}
		wg.Wait()
	})

}

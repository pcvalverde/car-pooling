package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

// Correct Cars JSON request with validation errors
var carsJson = []byte(`[
{"id": 100, "seats": 4},
{"id": 101, "seats": 5},
{"id": 102 },
{"id": 103, "seats": 3},
{"id": 101, "seats": 5}
]`)

func sendRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	Router.ServeHTTP(rr, req)
	return rr
}

func TestAPI(t *testing.T) {
	APIInit()

	// group JSON requests
	groupJson := [5][]byte{
		[]byte(`{"id": 1, "people": 4}`),
		[]byte(`{"id": 1, "people": 6}`),
		[]byte(`{"id": 2, "people": 8}`),
		[]byte(`{"id": 3, "people": 4`),
		[]byte(`{"id": 6, "people": 6}`),
	}

	t.Run("Check wrong header", func(te *testing.T) {
		req, _ := http.NewRequest("PUT", "/cars", nil)
		req.Header.Set("content-type", "application/xml")
		res := sendRequest(req)
		if res.Code != http.StatusBadRequest {
			t.Errorf("Accepted a wrong content-type header")
		}
	})

	t.Run("Send Correct Cars", func(te *testing.T) {
		req, _ := http.NewRequest("PUT", "/cars", bytes.NewBuffer(carsJson))
		req.Header.Set("content-type", "application/json")
		res := sendRequest(req)
		if res.Code != http.StatusOK {
			t.Errorf("Correct put cars request not accepted")
		}
	})

	t.Run("Send Group Requests", func(te *testing.T) {
		var res [5]*httptest.ResponseRecorder
		for i := 0; i < 5; i++ {
			req, _ := http.NewRequest("POST", "/journey", bytes.NewBuffer(groupJson[i]))
			req.Header.Set("content-type", "application/json")
			res[i] = sendRequest(req)
		}
		if res[0].Code != http.StatusOK {
			t.Errorf("Correct post journey request not accepted")
		}
		for i := 1; i < 4; i++ {
			if res[i].Code != http.StatusBadRequest {
				t.Errorf("Request %d not properly rejected. Code: %d", i, res[i].Code)
			}
		}
	})

	t.Run("Get location for groups", func(te *testing.T) {

		idsForm := [3]string{"ID=1", "ID=6", "ID=5"}
		var res [3]*httptest.ResponseRecorder
		for i := 0; i < 3; i++ {
			req, _ := http.NewRequest("POST", "/locate", bytes.NewBuffer([]byte(idsForm[i])))
			req.Header.Set("content-type", "application/x-www-form-urlencoded")
			res[i] = sendRequest(req)
		}
		if res[0].Code != http.StatusOK {
			t.Errorf("Car for group 1 not retrieved")
		}
		if res[1].Code != http.StatusNoContent {
			t.Errorf("Group 6 has not car assigned")
		}
		if res[2].Code != http.StatusNotFound {
			t.Errorf("Group 4 must not to be found")
		}
	})

	t.Run("Send Group DropOff", func(te *testing.T) {

		var idForm1 string = "ID=1"
		req, _ := http.NewRequest("POST", "/dropoff", bytes.NewBuffer([]byte(idForm1)))
		req.Header.Set("content-type", "application/x-www-form-urlencoded")
		res := sendRequest(req)
		if res.Code != http.StatusOK {
			t.Errorf("Dropoff of car 1 failed")
		}
		req, _ = http.NewRequest("POST", "/dropoff", bytes.NewBuffer([]byte(idForm1)))
		req.Header.Set("content-type", "application/x-www-form-urlencoded")
		res = sendRequest(req)
		if res.Code != http.StatusNotFound {
			t.Errorf("Expected dropoff returning car not found")
		}

	})

}

func sendJourney(id uint32) {
	var res *httptest.ResponseRecorder

	v := rand.Intn(5) + 1
	jsonGroup := fmt.Sprintf(`{"id": %d, "people": %d}`, id, v)
	req, _ := http.NewRequest("POST", "/journey", bytes.NewBuffer([]byte(jsonGroup)))
	req.Header.Set("content-type", "application/json")
	res = sendRequest(req)
	if res.Code != http.StatusOK {
		panic(fmt.Sprintf("Group %d with %d person panic", id, v))
	}
}

func sendLocate(id uint32) (code int) {
	var res *httptest.ResponseRecorder
	idForm := []byte(fmt.Sprintf("ID=%d", id))
	req, _ := http.NewRequest("POST", "/locate", bytes.NewBuffer(idForm))
	req.Header.Set("content-type", "application/x-www-form-urlencoded")
	res = sendRequest(req)
	if res.Code != http.StatusOK && res.Code != http.StatusNoContent {
		log.Printf("Car %d not found", id)
	}
	return res.Code
}

func sendDropoff(id uint32) (code int) {
	var idForm string = fmt.Sprintf("ID=%d", id)
	req, _ := http.NewRequest("POST", "/dropoff", bytes.NewBuffer([]byte(idForm)))
	req.Header.Set("content-type", "application/x-www-form-urlencoded")
	res := sendRequest(req)
	return res.Code
}

func TestAPICalls(t *testing.T) {
	APIInit()
	const maxCars = 10000
	const maxGroups = maxCars * 10
	t.Run("Integration Test", func(te *testing.T) {
		start := time.Now()
		// Code to measure

		cars := generateCars(maxCars)
		jsonCars, _ := json.Marshal(&cars)
		req, _ := http.NewRequest("PUT", "/cars", bytes.NewBuffer(jsonCars))
		req.Header.Set("content-type", "application/json")
		res := sendRequest(req)
		duration := time.Since(start)
		log.Printf(" Put %d cars in %s with code %d", maxCars, duration, res.Code)

		var count = seatsData.bySize[4].Size() + seatsData.bySize[5].Size() + seatsData.bySize[6].Size()
		if count != maxCars {
			te.Errorf("Expected %d seats and created %d", maxCars, count)
		}

		start = time.Now()
		for i := 1; i <= maxGroups; i++ {
			go sendJourney(uint32(i))
		}
		duration = time.Since(start)
		log.Printf(" Post %d journeys in %s", maxGroups, duration)

		//Wait for goroutines to finish for test validation (and avoid the use of Wait groups)
		time.Sleep(1 * time.Second)
		fullCars := seatsData.bySize[0].Size()
		if fullCars != maxCars {
			te.Errorf("Expected all cars full, but there are %d", maxCars-fullCars)
		}

		start = time.Now()
		locatedCars := 0
		withoutCar := 0
		for i := 1; i <= maxGroups; i++ {
			res := sendLocate(uint32(i))
			if res == http.StatusOK {
				locatedCars++
			}
			if res == http.StatusNoContent {
				withoutCar++
			}
		}
		duration = time.Since(start)
		log.Printf("Post %d locate in %s with %d located and %d without car",
			maxGroups, duration, locatedCars, withoutCar)

		start = time.Now()
		dropGroups := 0
		for i := 1; i <= maxGroups; i++ {
			res := sendDropoff(uint32(i))
			if res == http.StatusOK {
				dropGroups++
			}
		}
		duration = time.Since(start)
		log.Printf("Post %d dropoffs in %s with %d dropped groups",
			maxGroups, duration, dropGroups)

		count = seatsData.bySize[4].Size() + seatsData.bySize[5].Size() + seatsData.bySize[6].Size()
		if count != maxCars {
			te.Errorf("Expected available %d cars, but was %d", maxCars, count)
		}

	})
}

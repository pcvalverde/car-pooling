# Car Pooling Service Challenge

----
[[_TOC_]]

----
## Goal
This repository implements a solution to a Car Pooling Service Challenge using Go. The solution consists
on a REST API with a backend that stores the required structures in Go Data Structures

## Getting Started
The project uses GO 1.16 with the popular gorilla/mux package as only dependency. 
From the root folder in a system with the GOPATH properly configured, the following
commands build and  launch the API in port 9091
```bash
go get github.com/gorilla/mux
go build -o ./car-pooling-challenge .
./car-pooling-challenge
```
Also, the goDockerfile is provided for a self-contained deployment using docker build & run.

## Architectural decisions
- As the original documentation recommends or implicitly suggests not to use a database, the data storage of the
service is based on Sync.Map and a custom implementation of a concurrent FIFO Queue. First, Sync.Map 
  guarantees concurrent-safety over regular Go Map. At this moment, there is not an implementation of
  FIFO queues in the standard library. The only stable solution found was [the following one](https://github.com/enriquebris/goconcurrentqueue) but
  it uses slices and implicit lock checking. It was implemented a linked list approach to have a fine
  -grain control of the memory allocation. However, in a real environment, a memory-based database
  like Redis or Memcached would be a more sound approach to deal with concurrency and persistence.

- Two main structures have been defined to manage the data: 
  1. **Seats** provide an array of queues (BySize) in which each index correspond to the list of cars with given seat availability. 
     Therefore, a car with 6 seats is initially assigned to the Queue BySize[6]. If a group of 4 people is added to the car, the car reference is 
     moved to the Queue BySize[2]. This approach provides the retrieval of a car with n seats available in O(K). The map ByCar 
     stores for each car id the current seats and ptr to its specific position in the BySize Queues. This ptr helps
     to delete and move the car among queues also in O(K)
  2. **Journeys** provide a map to store the group information by its id and a waitlist Queue. When no car is found for a 
  given group, it is added to this ordered waitlist. When a slot in a car is available the cost to iterate an assign a new group is O(n). However its a straightfoward way to guarantee the arrival order of the groups.
     
- Regarding requirements, some assumptions are made to establish when new seats are available to be assigned to waiting
groups. First, the PUT /cars request is consider to reset the service to an initial state. That implies that already
  waiting groups from the Seats waitlist are also deleted. With the provided instructions, there are no means to determine when a ride starts or finishes 
  (when a car is full? when there is no group that can be assigned? after every dropoff?) as a dropoff could happen in both states. It is just
  assumed that when a dropoff happens the seats are available to be assigned to a waiting group or free for future requests.
  This assumption is made even there is another group assigned in the car. However changing this behaviour in the journeys
  DropGroup function is quite straightforward.
  
## Testing
Associated test files using the Go Testing Library are provided. They could be run with the go test command. Additional
tests of concurrent request for queues and cars have been implement using goroutines. As  main evaluation to check if the service
works as expected the test "TestAPICalls " from the API_test.go, initializes 10^4 cars and 10^5 journeys as described in the
requirements. Then, a call to locate and dropoff is made to every groupId. Using a httptest.Recorder the whole test 
should finish in lest than 10 seconds.

## Requirements
Design/implement a system to manage car pooling.

You have been assigned to build the car availability service that will be used
to track the available seats in cars.

Cars have a different amount of seats available, they can accommodate groups of
up to 4, 5 or 6 people.

People request cars in groups of 1 to 6. People in the same group want to ride
on the same car. You can take any group at any car that has enough empty seats
for them. If it's not possible to accommodate them, they're willing to wait until 
there's a car available for them. Once a car is available for a group
that is waiting, they should ride. 

Once they get a car assigned, they will journey until the drop off, you cannot
ask them to take another car (i.e. you cannot swap them to another car to
make space for another group).

In terms of fairness of trip order: groups should be served as fast as possible,
but the arrival order should be kept when possible.
If group B arrives later than group A, it can only be served before group A
if no car can serve group A.

For example: a group of 6 is waiting for a car and there are 4 empty seats at
a car for 6; if a group of 2 requests a car you may take them in the car.
This may mean that the group of 6 waits a long time,
possibly until they become frustrated and leave.


### API

To simplify the challenge and remove language restrictions, this service must
provide a REST API which will be used to interact with it.

This API must comply with the following contract:

### GET /status

Indicate the service has started up correctly and is ready to accept requests.

Responses:

* **200 OK** When the service is ready to receive requests.

### PUT /cars

Load the list of available cars in the service and remove all previous data
(existing journeys and cars). This method may be called more than once during 
the life cycle of the service.

**Body** _required_ The list of cars to load.

**Content Type** `application/json`

Sample:

```json
[
  {
    "id": 1,
    "seats": 4
  },
  {
    "id": 2,
    "seats": 6
  }
]
```

Responses:

* **200 OK** When the list is registered correctly.
* **400 Bad Request** When there is a failure in the request format, expected
  headers, or the payload can't be unmarshalled.

### POST /journey

A group of people requests to perform a journey.

**Body** _required_ The group of people that wants to perform the journey

**Content Type** `application/json`

Sample:

```json
{
  "id": 1,
  "people": 4
}
```

Responses:

* **200 OK** or **202 Accepted** When the group is registered correctly
* **400 Bad Request** When there is a failure in the request format or the
  payload can't be unmarshalled.

### POST /dropoff

A group of people requests to be dropped off. Whether they traveled or not.

**Body** _required_ A form with the group ID, such that `ID=X`

**Content Type** `application/x-www-form-urlencoded`

Responses:

* **200 OK** or **204 No Content** When the group is unregistered correctly.
* **404 Not Found** When the group is not to be found.
* **400 Bad Request** When there is a failure in the request format or the
  payload can't be unmarshalled.

### POST /locate

Given a group ID such that `ID=X`, return the car the group is traveling
with, or no car if they are still waiting to be served.

**Body** _required_ A url encoded form with the group ID such that `ID=X`

**Content Type** `application/x-www-form-urlencoded`

**Accept** `application/json`

Responses:

* **200 OK** With the car as the payload when the group is assigned to a car.
* **204 No Content** When the group is waiting to be assigned to a car.
* **404 Not Found** When the group is not to be found.
* **400 Bad Request** When there is a failure in the request format or the
  payload can't be unmarshalled.
  

### Requirements

- The service should be as efficient as possible.
  It should be able to work reasonably well with at least $`10^4`$ / $`10^5`$ cars / waiting groups.
  Explain how you did achieve this requirement.
- You are free to modify the repository as much as necessary to include or remove
  dependencies, subject to tooling limitations above.
- Document your decisions using MRs or in this very README adding sections to it,
  the same way you would be generating documentation for any other deliverable.
  We want to see how you operate in a quasi real work environment.
  
package main

import (
	"sync"
	"testing"
)

func TestQueue(t *testing.T) {
	q := Queue{}
	ids := []int{2, 8, 15, 3, 4}
	for _, id := range ids {
		q.Push(id)
	}
	t.Run("Push  elements", func(te *testing.T) {
		if q.size != 5 {
			te.Errorf("expected 5 elements, but was %d", q.size)
		}
	})
	t.Run("First Element", func(te *testing.T) {
		if first := q.Peek(); first.data != 2 {
			te.Errorf("expected 2 data, but was %d", first.data)
		}
	})
	t.Run("Pop elements", func(te *testing.T) {
		q.Pop()
		value := q.Pop().(int)
		if first := q.Peek(); first.data != 15 {
			te.Errorf("expected 15 as first data, but was %d", first.data)
		}
		if value != 8 {
			te.Errorf("expected 8 as pop data, but was %d", value)
		}
		if q.size != 3 {
			te.Errorf("expected 3 elements, but was %d", q.size)
		}
	})
	t.Run("Delete queue elements", func(te *testing.T) {
		q := Queue{}
		var node = q.Push(1)
		q.Delete(node)
		if q.head != nil || q.tail != nil {
			te.Errorf("Queue with a single element not properly removed")
		}

		q = Queue{}
		var nodes [10]*Node
		for i := 0; i < 10; i++ {
			nodes[i] = q.Push(i)
		}

		q.Delete(nodes[0])
		if q.head.data.(int) != 1 {
			te.Errorf("The head node has not been properly removed")
		}
		q.Delete(nodes[9])
		if q.tail.data.(int) != 8 {
			te.Errorf("The tail node has not been properly removed")
		}
		q.Delete(nodes[2])
		if q.head.next.data.(int) != 3 {
			te.Errorf("The 2th node has not been properly removed")
		}
		if q.Size() != 7 {
			te.Errorf("Queue size expected 7, but was %d", q.Size())
		}
	})

	t.Run("Adding elements concurrently", func(te *testing.T) {
		q := Queue{}
		const routines = 1000
		var wg sync.WaitGroup
		wg.Add(routines)
		var (
			concurrent = func(id int) {
				defer wg.Done()
				q.Push(id)
				q.Pop()
				q.Push(id)
			}
		)
		for i := 0; i < routines; i++ {
			go concurrent(i)
		}
		wg.Wait()
		if q.size != routines {
			te.Errorf("expected %d elements, but was %d", routines, q.size)
		}
		q.Clear()
		if q.size != 0 {
			te.Errorf("expected 0 elements, but was %d", q.size)
		}
	})

}

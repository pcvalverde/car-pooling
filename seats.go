package main

import (
	"fmt"
	"sync"
)

const MaxSeats = 6

// Seats manages the seats available in the service using an array of Queues and a Map
type Seats struct {
	//An array of queues in which each position represents a list of car with tha seat
	//availability
	bySize [MaxSeats + 1]Queue
	//Map to look for information for a given car Id as key
	byCar   sync.Map
	rwMutex sync.RWMutex
}

// carSeats represents the values of the ByCar map
type carSeats struct {
	//total seats of the map
	total uint8
	//available seats always <= total
	available uint8
	//ptr to the specific BySize queue position
	sizeQueue *Node
}

// Car represents the json items expected in the PUT /car endpoint
type Car struct {
	Id    uint32 `json:"id"`
	Seats uint8  `json:"seats"`
}

// validate checks if the seats value of Car struct complies the requirement
func (c *Car) validate(seats *Seats) (err error) {
	if c.Id < 1 {
		return fmt.Errorf("Rejected car with wrong id %d", c.Id)
	}
	if c.Seats < 4 || c.Seats > 6 {
		return fmt.Errorf("Car %d has %d seats. Expected 4-6", c.Id, c.Seats)
	}
	if _, found := seats.byCar.Load(c.Id); found {
		return fmt.Errorf("A car with id %d has been previously created", c.Id)
	}
	return err
}

//NewSeats creates an struct of seats available according to an array of cars
//Any validation error found is returned in the errors array
func NewSeats(cars []Car) (seats *Seats, errors []error) {
	seats = &Seats{}
	for i := 0; i < len(cars); i++ {
		if err := cars[i].validate(seats); err != nil {
			errors = append(errors, err)
			continue
		}
		var seatsCap = cars[i].Seats
		var carSeats = carSeats{seatsCap, seatsCap, nil}
		seats.byCar.Store(cars[i].Id, carSeats)
		var node = seats.bySize[seatsCap].Push(cars[i].Id)
		carSeats.sizeQueue = node
	}
	return seats, errors
}

// BookCar returns a Car Id with at least the required Seats or false if not found. If there
// are remaining Seats, adds the Car to the corresponding index of the bySize queue
func (s *Seats) BookCar(seats uint8) (availCar uint32, found bool) {
	var seatCap = seats
	for ; seatCap <= MaxSeats; seatCap++ {
		var item = s.bySize[seatCap].Pop()
		if item != nil {
			availCar = item.(uint32)
			found = true
			break
		}
	}
	if availCar != 0 {
		var availSeats = seatCap - seats
		var cs, _ = s.byCar.Load(availCar)
		var carSeats = cs.(carSeats)
		carSeats.sizeQueue = s.bySize[availSeats].Push(availCar)
		carSeats.available = availSeats
		s.byCar.Store(availCar, carSeats)
	}
	return availCar, found
}

//ByCarId returns the available Seats for a given CarId or not found
func (s *Seats) ByCarId(carId uint32) (uint8, bool) {
	var availSeats uint8
	var cs, found = s.byCar.Load(carId)
	if found {
		var idSeats = cs.(carSeats)
		availSeats = idSeats.available
	}
	return availSeats, found
}

//ByCarId returns the available Seats for a given CarId or not found
func (s *Seats) GetCar(carId uint32) (car *Car, found bool) {
	cs, found := s.byCar.Load(carId)
	if found {
		var idSeats = cs.(carSeats)
		car = &Car{carId, idSeats.total}
	}
	return car, found
}

//Free assigns the a number of available Seats for the Car Id and updates the data structures
func (s *Seats) Free(carId uint32, seats uint8) (err error) {
	s.rwMutex.Lock()
	defer s.rwMutex.Unlock()
	if seats > 6 {
		return fmt.Errorf("Expected to free a maximum of 6 seats for car %d, but was %d", carId, seats)
	}
	var cs, found = s.byCar.Load(carId)
	if !found {
		return fmt.Errorf("Car with id %d not found for free seats", carId)
	}
	var carSeats = cs.(carSeats)
	s.bySize[carSeats.available].Delete(carSeats.sizeQueue)
	carSeats.sizeQueue = s.bySize[seats].Push(carId)
	carSeats.available = seats
	s.byCar.Store(carId, carSeats)
	return err
}

//Clear removes all available Seats for each queue and the whole map
//Required to guarantee that memory is freed by the GC
func (s *Seats) Clear() {
	if s == nil {
		return
	}
	for i := 1; i <= MaxSeats; i++ {
		s.bySize[i].Clear()
	}
	s.byCar.Range(func(key interface{}, value interface{}) bool {
		s.byCar.Delete(key)
		return true
	})
}

// Show prints the number of cars bySize for each number of Seats
func (s *Seats) Show() {
	for i := 1; i <= MaxSeats; i++ {
		var q *Queue = &(s.bySize[i])
		fmt.Printf("%d cars with %d Seats ", q.size, i)
		q.Display()
	}
}

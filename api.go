package main

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

var Router = mux.NewRouter()
var seatsData *Seats
var journeysData *Journeys

func logErr(n int, err error) {
	if err != nil {
		log.Printf("Write failed: %v", err)
	}
}
func APIInit() {
	seatsData = &Seats{}
	journeysData = New(seatsData)
	Router.HandleFunc("/status", getStatusHandler).Methods("GET")
	Router.HandleFunc("/cars", putCarsHandler).Methods("PUT")
	Router.HandleFunc("/journey", postJourneyHandler).Methods("POST")
	Router.HandleFunc("/dropoff", postDropoffHandler).Methods("POST")
	Router.HandleFunc("/locate", postLocateHandler).Methods("POST")
}

//getStatusHandler Indicate the service has started up correctly and is ready to accept requests.
//Responses:
//200 OK When the service is ready to receive requests.
func getStatusHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	logErr(w.Write([]byte("Car pooling server ready!!")))
}

// sendResponseErr sends an Ok response and includes validations error messages
// in the body. Used for sending validation errors in Car struct (wrong number of seats)
func sendResponseErr(w http.ResponseWriter, errorsMsg []error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	var errorsMap = make(map[int]string)
	for i, err := range errorsMsg {
		errorsMap[i] = err.Error()
	}
	jsonResp, _ := json.Marshal(errorsMap)
	logErr(w.Write(jsonResp))
}

// sendHTTPError creates and sends and HTTP.Error instance with error messages in the body
func sendHTTPError(err error, w *http.ResponseWriter) {
	var mr *malformedRequest
	if errors.As(err, &mr) {
		http.Error(*w, mr.msg, mr.status)
	} else {
		log.Println(err.Error())
		http.Error(*w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
}

//putCarsHandler loads the list of available cars in the service and remove all previous data
//(existing journeys and cars).
//This method may be called more than once during the life cycle of the service.
//Body required The list of cars to load.
//Content Type application/json
//Responses:
//200 OK When the list is registered correctly.
//400 Bad Request When there is a failure in the request format, expected headers, or the payload can't be unmarshalled.
func putCarsHandler(w http.ResponseWriter, r *http.Request) {
	var cars []Car
	err := decodeJSONBody(w, r, &cars)
	if err != nil {
		sendHTTPError(err, &w)
		return
	}
	var errs []error
	seatsData.Clear()
	seatsData, errs = NewSeats(cars)
	journeysData.Clear()
	journeysData = New(seatsData)
	sendResponseErr(w, errs)
}

//postJourneyHandler A group of people requests to perform a journey.
//Body required The group of people that wants to perform the journey
//Content Type application/json
//Sample: { "id": 1, "people": 4 }
//Responses:
//200 OK or 202 Accepted When the group is registered correctly
//400 Bad Request When there is a failure in the request format or the payload can't be unmarshalled.
func postJourneyHandler(w http.ResponseWriter, r *http.Request) {
	var group Group
	err := decodeJSONBody(w, r, &group)
	if err != nil {
		sendHTTPError(err, &w)
		return
	}
	err = journeysData.Add(group)
	w.Header().Set("Content-Type", "application/json")

	var jsonResp []byte
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonResp, _ = json.Marshal(err.Error())
	} else {
		w.WriteHeader(http.StatusOK)
		//Acceptance tests do not support content in body response
		//resp := fmt.Sprintf("Group with id %d added",group.Id)
		//jsonResp, _ = json.Marshal(resp)
	}
	logErr(w.Write(jsonResp))
}

// checkIdForm parses the content of the request body looking for a valid int ID
func checkIdForm(w http.ResponseWriter, r *http.Request) (id uint32) {

	if r.Header.Get("Content-Type") != "application/x-www-form-urlencoded" {
		msg := "Content-Type header is not application/x-www-form-urlencoded"
		sendHTTPError(&malformedRequest{http.StatusBadRequest, msg}, &w)
		return id
	}
	err := r.ParseForm()
	if err != nil {
		msg := "Error parsing form body"
		sendHTTPError(&malformedRequest{http.StatusBadRequest, msg}, &w)
		return id
	}
	strId, found := r.PostForm["ID"]
	if !found {
		msg := "Form body does not contain an ID Value"
		sendHTTPError(&malformedRequest{http.StatusBadRequest, msg}, &w)
		return id
	}
	intId, err := strconv.ParseUint(strId[0], 10, 32)
	if err != nil {
		msg := "ID does not contains a valid int value"
		sendHTTPError(&malformedRequest{http.StatusBadRequest, msg}, &w)
		return
	}
	id = uint32(intId)
	if id == 0 {
		msg := "ID with 0 value is no allowed"
		sendHTTPError(&malformedRequest{http.StatusBadRequest, msg}, &w)
		return
	}
	return id
}

//postDropoffHandler A group of people requests to be dropped off. Whether they traveled or not.
//Body required A form with the group ID, such that ID=int
//Content Type application/x-www-form-urlencoded
//Responses:
//200 OK or 204 No Content When the group is unregistered correctly.
//404 Not Found When the group is not to be found.
//400 Bad Request When there is a failure in the request format or the payload can't be unmarshalled.
func postDropoffHandler(w http.ResponseWriter, r *http.Request) {
	var id uint32
	if id = checkIdForm(w, r); id == 0 {
		return
	}
	err := journeysData.DropGroup(id)
	if err != nil {
		sendHTTPError(&malformedRequest{http.StatusNotFound, err.Error()}, &w)
		return
	}
	w.WriteHeader(http.StatusOK)
}

//postLocateHandler Given a group ID such that ID=X, return the car the group is traveling with,
//or no car if they are still waiting to be served.
//Body required A url encoded form with the group ID such that ID=X
//Content Type application/x-www-form-urlencoded
//Accept application/json
//Responses:
//200 OK With the car as the payload when the group is assigned to a car.
//204 No Content When the group is waiting to be assigned to a car.
//404 Not Found When the group is not to be found.
//400 Bad Request When there is a failure in the request format or the payload can't be unmarshalled.
func postLocateHandler(w http.ResponseWriter, r *http.Request) {
	var id uint32
	if id = checkIdForm(w, r); id == 0 {
		return
	}
	car, found := journeysData.GetGroupCar(id)
	if !found {
		w.WriteHeader(http.StatusNotFound)
		//msg := "Group not found"
		//sendHTTPError(&malformedRequest{http.StatusNotFound, ""}, &w)
		return
	}
	if car == nil {
		w.WriteHeader(http.StatusNoContent)
		//msg := "Group has not car assigned"
		//sendHTTPError(&malformedRequest{http.StatusNoContent, msg}, &w)
		return
	}
	jsonResp, _ := json.Marshal(car)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	logErr(w.Write(jsonResp))
}

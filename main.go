package main

import (
	"log"
	"net/http"
)

func main() {
	APIInit()
	log.Fatal(http.ListenAndServe(":9091", Router))
}

package main

import "testing"

func TestJourney(t *testing.T) {

	var cars2add = []Car{
		{1, 6},
		{2, 4},
	}

	var mySeats, _ = NewSeats(cars2add)
	journeys := New(mySeats)

	journeys.Add(Group{1, 4})
	journeys.Add(Group{2, 2})
	journeys.Add(Group{3, 6})
	journeys.Add(Group{4, 3})

	t.Run("Check group assignments", func(te *testing.T) {
		car, found := journeys.GetGroupCar(1)
		if !found || car.Id != 2 {
			te.Errorf("Expected group 1 assigned to car 2")
		}
		car, found = journeys.GetGroupCar(3)
		if !found || car.Id != 0 {
			te.Errorf("Expected group 3 has not car assigned")
		}
		if mySeats.bySize[1].Size() != 1 {
			te.Error("Expected at least one car with seats available")
		}
	})

	t.Run("Avoid groups with errors", func(te *testing.T) {
		err := journeys.Add(Group{1, 4})
		if err == nil {
			te.Error("Group with existing id not rejected")
		}
		err = journeys.Add(Group{10, 40})
		if err == nil {
			te.Error("Group with more than 6 people not rejected")
		}
	})

	t.Run("Drop Groups", func(te *testing.T) {
		journeys.DropGroup(2)
		journeys.DropGroup(4)

		if _, found := journeys.get(2); found {
			te.Error("Group with id 2 not dropped")
		}
		car, found := journeys.GetGroupCar(3)
		if !found || car.Id != 1 {
			te.Error("Car with id 1 not assigned to 6-people waiting group")
		}
		journey, _ := journeys.get(3)
		if journey.waitPos != nil {
			te.Error("6 people waiting group not removed from waitlist")
		}
		if mySeats.bySize[0].Size() != 2 {
			te.Error("It should be two cars without any seats available")
		}
	})

}
